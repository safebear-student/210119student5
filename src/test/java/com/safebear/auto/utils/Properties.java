package com.safebear.auto.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Properties {

    public static String generateScreenShotFileName(){
        //create filename
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
    }

    public static void capturescreenshot(WebDriver driver, String fileName){
        //Take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        //Make sure that the 'screenshots' directory exists
        File file = new File("target/screenshot");
        if (!file.exists()){
            if (file.mkdir()){
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }

        // Copy file to filename and location we set before
        try {
            copy(scrFile, new File ("target/screenshot" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void captureElementScreenshot(WebDriver driver, By locator, String fileName) throws Exception
    {
        WebElement ele = driver.findElement(locator);
        Screenshot screenshot = new AShot().takeScreenshot(driver,ele);
        ImageIO.write(screenshot.getImage(),"PNG",new File(System.getProperty("user.dir") + fileName + ".png"));
    }

    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
 //   private static final String URL = System.getProperty("url", "http://localhost:8080");
 //   private static final String BROWSER = System.getProperty("browser", "headless");
    private static final String BROWSER = System.getProperty("browser","chrome");

    public static String getUrl () {
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("window=size=1366, 768");

        FirefoxOptions firefoxoptions = new FirefoxOptions();
        options.addArguments("window=size=1366,768");



        switch (BROWSER) {
            case "chrome":
               return new ChromeDriver(options);

            case "firefox":
                return new FirefoxDriver(firefoxoptions);

            case "headless":
                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);

            default:
                   return new ChromeDriver(options);
               // return new FirefoxDriver(firefoxoptions);

        }
    }
}
