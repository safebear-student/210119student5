package com.safebear.auto.tests;


import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Properties;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp(){
        driver = Properties.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown(){
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.get(Properties.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle(), "We're not on the Login Page or its title has changed");
    }

    @When("^I enter the login details '(.+)'$")
    public void i_enter_the_login_details_for_a_User(String user) throws Throwable {
       switch (user) {
           case "validUser":
             //  loginPage.enterUsername("tester");
              // loginPage.enterPassword("letmein");
              // loginPage.clickLoginButton();
               loginPage.login( "tester",  "letmein");
               break;
           case "invalidUser":
             //  loginPage.enterUsername("attacker");
             //  loginPage.enterPassword("donletmein");
             //  loginPage.clickLoginButton();
               loginPage.login("attacker","donotletmein");
               break;
           default:
               Assert.fail("The test data is wrong - the only values that can be accepted are 'validuser' or 'invaliduser");
               break;
       }
    }

    @Then("^I can see the following message: '(.+)'$")
    public void i_can_see_the_following_message (String message) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        switch (message)
        {
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkforFailedLoginWarning().contains(message));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(message));
                break;
            default:
                Assert.fail ("The test data is wrong");
                break;
        }
    }

}
