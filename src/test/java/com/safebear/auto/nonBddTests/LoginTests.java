package com.safebear.auto.nonBddTests;

import com.safebear.auto.utils.Properties;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test
    public void validLogin(){
        // 1.Action Go to login page
        driver.get(Properties.getUrl());
        // 1.Expected check I'm on login
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        // 2.Action log in as a valid user
        loginPage.login("tester", "letmein");
        // 2.Expected Check I'm on Tools page
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page");
        // 3.Expected Check success message is shown
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Success"));
        Properties.capturescreenshot(driver,Properties.generateScreenShotFileName());

        try {
            Properties.captureElementScreenshot(driver,toolsPageLocators.getSuccessMessageLocator(),"getsuccessmessage");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void invalidLogin(){
        //1.Action Go to login page
        driver.get(Properties.getUrl());
        //1.Expected check I'm on login
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        //2.Action login as a valid user
        loginPage.login("hacker","letmein");
        //2.Expected Check I'm still on login
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page");
        //3.Expected Check failed message is shown
        Assert.assertTrue(loginPage.checkforFailedLoginWarning().contains("incorrect"));
    }
}
