package com.safebear.auto.nonBddTests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.pages.locators.LoginPageLocators;
import com.safebear.auto.pages.locators.ToolsPageLocators;
import com.safebear.auto.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;
    LoginPageLocators loginPageLocators;
    ToolsPageLocators toolsPageLocators;

    @BeforeTest
    public void setUp(){
        driver = Properties.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
        loginPageLocators = new LoginPageLocators();
        toolsPageLocators = new ToolsPageLocators();
    }

    @AfterTest
    public void tearDown(){
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

}
