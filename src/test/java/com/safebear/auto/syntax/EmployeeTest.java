package com.safebear.auto.syntax;

import org.testng.annotations.Test;

@Test
public class EmployeeTest {
    public void testEmployee()

        {
            // This is where we create our objects
            Employee hannah = new Employee();
            Employee bob = new Employee();
            //Create a sales employee object
            SalesEmployee victoria = new SalesEmployee();
            OfficeEmployee toby = new OfficeEmployee();


            //This is where we employ hannah and fire bob
            hannah.employ();
            bob.fire();

            //This is where we employ victoria and give her a bmw
            victoria.employ();
            victoria.changeCar("bmw");

            //A new desk
            toby.employ();
            toby.changeDesk(2);


            //Let's print their state to screen
            System.out.println("Hannah employment state: " + hannah.isEmployed());
            System.out.println("Bob employment state: " + bob.isEmployed());
        //   System.out.println("Victoria salary state:" +victoria.setSalary(int Salary));
            System.out.println("Victoria's car:" + victoria.car);
            System.out.println("Victoria" +victoria.getSalary());
       //     System.out.println("Toby's car:" +toby.isEmployed();
        //    System.out.println("Toby's desk:" +toby.desk);
        }
}
