Feature: Login
  In order to access the website
  As a user
  I want to know if login is successful

  Rules:
  * User must be informed success
  * User must be informed if failed

  Glossary:
  * User: Someone who wants to create a Tools List using our application
  * Supporters:  This is what the customer calls 'Admin' users

  @HighRisk
  @HighImpact
    Scenario Outline: Navigate and login to the application
    Given I navigate to the login page
    When  I enter the login details '<userType>'
    Then  I can see the following message: '<validationMessage>'
    Examples:
    |  userType     |  validationMessage                  |
    |  invalidUser  |  Username or Password is incorrect  |
    |  validUser    |  Login Successful                   |