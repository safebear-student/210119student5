package com.safebear.auto.syntax;

public class Employee {
    private boolean employed;
    private int salary;

    public void fire(){
        employed = false;
    }

    public void employ(){
        employed = true;
    }

    public void setEmployed(boolean employed) {
        this.employed = employed;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void givePayRise(){
        salary = salary + 10;
    }

    public boolean isEmployed() {
        return employed;
    }
}
